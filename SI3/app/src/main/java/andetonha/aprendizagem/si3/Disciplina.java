package andetonha.aprendizagem.si3;

import java.io.Serializable;

/**
 * Created by Ygor on 20/08/2015.
 */
public class Disciplina implements Serializable {

    private String id;
    private String nome;
    private String local;
    private String horario;
    private String valorViewState;
    private String valorFormAcessarTurmaVirtual;
    private String valorHrefFormAcessarTurmaVirtual;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getValorViewState() {
        return valorViewState;
    }

    public void setValorViewState(String valorViewState) {
        this.valorViewState = valorViewState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValorFormAcessarTurmaVirtual() {
        return valorFormAcessarTurmaVirtual;
    }

    public void setValorFormAcessarTurmaVirtual(String valorFormAcessarTurmaVirtual) {
        this.valorFormAcessarTurmaVirtual = valorFormAcessarTurmaVirtual;
    }

    public String getValorHrefFormAcessarTurmaVirtual() {
        return valorHrefFormAcessarTurmaVirtual;
    }

    public void setValorHrefFormAcessarTurmaVirtual(String valorHrefFormAcessarTurmaVirtual) {
        this.valorHrefFormAcessarTurmaVirtual = valorHrefFormAcessarTurmaVirtual;
    }
}

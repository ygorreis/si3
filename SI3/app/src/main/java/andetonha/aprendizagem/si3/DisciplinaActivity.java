package andetonha.aprendizagem.si3;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DisciplinaActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disciplina);

        final List<Disciplina> lista = new ArrayList<Disciplina>();
        //final String[] cookies = getIntent().getExtras().getStringArray("cookies");
        List<String> nomes = new ArrayList<String>();
        int total = getIntent().getExtras().getInt("SIZE");
        for (int i = 0; i < total; i++){
            lista.add((Disciplina) getIntent().getSerializableExtra(String.valueOf(i)));
            nomes.add(lista.get(i).getNome());
        }

        ArrayList<Map<String,String>> cookies = (ArrayList<Map<String,String>>) getIntent().getSerializableExtra("array");
        final Map<String,String> cookiesOriginais = cookies.get(0);

        ListView lvDisc = (ListView) findViewById(R.id.listView_Disciplinas);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,nomes);
        lvDisc.setAdapter(adapter);

        lvDisc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "clicou", Toast.LENGTH_SHORT).show();

                final int posicao = i;
                Thread downloadContent = new Thread(){
                    @Override
                    public void run() {
                        try {
                            Disciplina a = lista.get(posicao);

                            //Mantêm sessão ativa
                            Document doc = Jsoup.connect("https://si3.ufc.br/sigaa/portais/discente/discente.jsf?")
                                    .data(a.getValorFormAcessarTurmaVirtual(), a.getValorFormAcessarTurmaVirtual(),
                                            "idturma", a.getId(),
                                            "javax.faces.ViewState", a.getValorViewState(),
                                            a.getValorHrefFormAcessarTurmaVirtual(), a.getValorHrefFormAcessarTurmaVirtual())
                                    .cookies(cookiesOriginais)
                                    .referrer("https://si3.ufc.br/sigaa/portais/discente/discente.jsf")
                                    .timeout(30000)
                                    .post();

                            Log.i("TAG", "HTML PROG SCRIPT: " + doc.getElementsByClass("intro-aval"));

                            System.out.println("***************************************************");
                            System.out.println(doc.html());
                            System.out.println("***************************************************");
                        }catch (Exception e){
                            Log.i("TAG", "ERROR: " + e.getMessage());
                        }
                    }
                };
                downloadContent.start();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_disciplina, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

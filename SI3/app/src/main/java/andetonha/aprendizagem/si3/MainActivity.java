package andetonha.aprendizagem.si3;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends ActionBarActivity {

    List<Disciplina> disciplinasDoAluno = new ArrayList<Disciplina>();
    Map<String, String> loginCookies;
    ArrayList<Map<String,String>> arl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.btn_go);
        final EditText user_login = (EditText) findViewById(R.id.et_user_login);
        final EditText user_senha = (EditText) findViewById(R.id.et_user_senha);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!user_login.getText().toString().isEmpty() && !user_senha.getText().toString().isEmpty()){
                    Thread downloadThread = new Thread(){
                        @Override
                        public void run() {
                            try {

                                //Requisição para pegar os cookies
                                Connection.Response res = Jsoup
                                        .connect("https://si3.ufc.br/sigaa/logar.do?dispatch=logOn")
                                        .data("user.login",user_login.getText().toString(),"user.senha",user_senha.getText().toString())
                                        .method(Connection.Method.POST)
                                        .execute();

                                //Pega os cookies
                                loginCookies = res.cookies();
                                arl = new ArrayList<Map<String, String>>();
                                arl.add(loginCookies);

                                //Mantêm sessão ativa
                                Document doc = Jsoup.connect("https://si3.ufc.br/sigaa/paginaInicial.do")
                                        .cookies(loginCookies)
                                        .get();

                                //captura a tabela de disciplina
                                Element painelDisciplinas = doc.getElementById("turmas-portal");

                                //captura todas as linhas da tabela
                                Elements painelDisciplinas2 = painelDisciplinas.getElementsByTag("tr");

                                //removendo a linha que contem o header da tabela
                                painelDisciplinas2.remove(0);

                                //size dividido por dois é o total de disciplinas
                                //System.out.println("QTDE disciplinas: " + painelDisciplinas2.size());

                                for(int i = 0; i < painelDisciplinas2.size(); i++){
                                    //SI3 cada tr é uma cadeira, mas ele coloca um tr vazio para preencher a tabela U.U
                                    if(i % 2 == 0){
                                        Element disciplinaRecuperada = painelDisciplinas2.get(i);
                                        Disciplina a = new Disciplina();

                                        //pegando o id da turma
                                        a.setId(disciplinaRecuperada.select("input[name=idTurma]").attr("value"));

                                        //pegando valor do input formAcessarTurmaVirtual
                                        a.setValorFormAcessarTurmaVirtual(disciplinaRecuperada.select("input").first().attr("value"));

                                        //pegando valor href pro href formAcessarTurmaVirtual
                                        a.setValorHrefFormAcessarTurmaVirtual(disciplinaRecuperada.select("a").first().attr("id"));

                                        //pegando o valor do state view
                                        a.setValorViewState(disciplinaRecuperada.select("input[name=javax.faces.ViewState]").attr("value"));

                                        //pegando o nome da disciplina
                                        a.setNome(disciplinaRecuperada.getElementsByClass("descricao").text());

                                        //pegando o horário e o local da disciplina
                                        Elements info = disciplinaRecuperada.getElementsByClass("info");
                                        a.setLocal(info.get(0).text());
                                        a.setHorario(info.get(1).text());

                                        disciplinasDoAluno.add(a);

                                    }
                                }

                            }catch (Exception e){
                                Log.i("TAG", "ERROR: " + e.getMessage());
                                MainActivity.super.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Login e/ou senha são inválidos", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    };
                    downloadThread.start();

                    try {
                        downloadThread.join();
                        //System.out.println("QTDE DISCIPLINAS: " + disciplinasDoAluno.size());
                        if(disciplinasDoAluno.size() > 0 ){
                            Intent intent = new Intent(getApplicationContext(), DisciplinaActivity.class);
                            intent.putExtra("SIZE", disciplinasDoAluno.size());
                            for (int i = 0; i < disciplinasDoAluno.size(); i++){
                                intent.putExtra(String.valueOf(i), disciplinasDoAluno.get(i));
                            }
                            intent.putExtra("array",arl);
                            startActivity(intent);
                        }else{
                            System.out.println("Não há disciplinas");
                        }
                    }catch (InterruptedException ie){
                        Log.i("TAG", "ERROR, THREAD JOIN: " + ie.getMessage());
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Login e/ou senha estão em branco", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
